// Remember to delete ./project/target before each test

// Uncomment and it will NOT work
// scalaVersion := "2.13.1"

// Uncomment and it WILL work
// ThisBuild / scalaVersion := "2.13.1"


// Comment the subproject definition and it WILL work even with globally defined scalaVersion
lazy val subproject = project
  .in(file("sub"))
  .dependsOn(mainProject)
  .settings(
    name := "Subproject",
    description := "Subproject example",
    moduleName := "subproject"
  )

lazy val mainProject = project
  .in(file("."))
  .settings(
    name := "MainProject",
    description := "Main project example",
  )
